from random import randint

# FINISH THE EXTRA CHALLENGES GIVEN BEFORE CLASS TOMORROW

# Name prompt
name = input("Hi! What is your name? ")

# Computer guesses numbers
guess_number = 1

low_month = 1
low_year = 1924
high_month = 12
high_year = 2004



for guess_number in range(1, 6):
    # Month and Year
    guess_month = randint(low_month, high_month)
    guess_year = randint(low_year, high_year)


    # Prompt
    print("Guess", guess_number, name, " were you born in ",
        guess_month, " / ", guess_year, " ?")
    # Response
    response = input("yes, later, or earlier? ")
    #
    if response == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other thigns to do. Good bye.")
    elif response == "later":
        low_month = guess_month + 1
        low_year = guess_year + 1
    elif response == "earlier":
        high_month = guess_month - 1
        high_month = guess_year - 1
    else:
        print("Drat! Lemme try again!")
